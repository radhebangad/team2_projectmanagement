package com.team2_portfoliomanagement.demo.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TransactionTest {
    @Test
    public void testTransactionGettersAndSetters() {
        Transaction testTransaction = new Transaction();
        testTransaction.setTicker("AAPL");
        testTransaction.setId("Apple");
        testTransaction.setOrderPrice(148.4);
        testTransaction.setOrderSize(4);
        testTransaction.setOrderType("BUY");

        assertEquals("AAPL", testTransaction.getTicker());
        assertEquals("Apple", testTransaction.getId());
        assertEquals(148.4, testTransaction.getOrderPrice());
        assertEquals(4, testTransaction.getOrderSize());
        assertEquals("BUY", testTransaction.getOrderType());
    }
}
