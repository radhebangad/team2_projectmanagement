package com.team2_portfoliomanagement.demo.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CashAccountTest {

    @Test
    void getId() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setId(123);
        assertEquals(cashAccountTest.getId(), 123);
    }

    @Test
    void setId() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setId(123);
        assertEquals(cashAccountTest.getId(), 123);
    }

    @Test
    void getAcctNumber() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setAcctNumber("123-456");
        assertEquals(cashAccountTest.getAcctNumber(), "123-456");
    }

    @Test
    void setAcctNumber() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setAcctNumber("123-456");
        assertEquals(cashAccountTest.getAcctNumber(), "123-456");
    }

    @Test
    void getName() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setName("RBC Checking");
        assertEquals(cashAccountTest.getName(), "RBC Checking");
    }

    @Test
    void setName() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setName("RBC Checking");
        assertEquals(cashAccountTest.getName(), "RBC Checking");
    }

    @Test
    void getBalance() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setBalance(1000);
        assertEquals(cashAccountTest.getBalance(), 1000);
    }

    @Test
    void setBalance() {
        CashAccount cashAccountTest = new CashAccount();
        cashAccountTest.setBalance(1000);
        assertEquals(cashAccountTest.getBalance(), 1000);
    }
}