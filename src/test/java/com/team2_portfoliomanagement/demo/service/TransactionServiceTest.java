package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import com.team2_portfoliomanagement.demo.model.Transaction;
import com.team2_portfoliomanagement.demo.repository.TransactionRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TransactionServiceTest {

    @Autowired
    private TransactionService transactionService;

    @MockBean
    private TransactionRepository transactionRepository;

    @Test
    public void testStockServiceFindAll() {
        List<Transaction> allTransactions = new ArrayList<Transaction>();
        Transaction testTransaction = new Transaction();
        testTransaction.setId("1234");
        allTransactions.add(testTransaction);

        when(transactionRepository.findAll()).thenReturn(allTransactions);

        assertEquals(1, transactionService.findAll().size());
    }

    @Test
    public void transactionServiceFindByIdTest(){
        HashMap<String, Optional<Transaction>> allTransactions = new HashMap<>();
        Transaction transaction = new Transaction();

        Optional<Transaction> optionalTransaction = Optional.of(transaction);
        transaction.setId("123");
        allTransactions.put("123", optionalTransaction);

        when(transactionRepository.findById("123")).thenReturn(allTransactions.get("123"));

        assertTrue(transactionService.findById("123").isPresent());
    }

    @Test
    public void  transactionServiceDeleteTest(){
        HashMap<String, Transaction> allTransactions = new HashMap<>();
        Transaction testTransaction = new Transaction();

        testTransaction.setId("123");
        allTransactions.put("123", testTransaction);

        doNothing().when(transactionRepository).deleteById(isA(String.class));
        allTransactions.remove("123");

        transactionService.delete("123");
        assertEquals(0, allTransactions.size());
    }

    @Test
    public void transactionServiceSaveTest(){
        Transaction testTransaction = new Transaction();

        testTransaction.setId("123");

        when(transactionRepository.save(testTransaction)).thenReturn(testTransaction);

        assertEquals(testTransaction.getId(), transactionService.save(testTransaction).getId());
    }

}
