package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class CashAccountServiceIntegrationTest {

    @Autowired
    private CashAccountService cashAccountService;

    @Test
    public void testCashAccountServiceFindAll() {
        List<CashAccount> allCashAccounts = cashAccountService.findAll();

        assertEquals(0, allCashAccounts.size()); // edit this when there are MongoDB Documents stored
        //System.out.println("AAPL"+ allCashAccounts.get(0));
    }

}
