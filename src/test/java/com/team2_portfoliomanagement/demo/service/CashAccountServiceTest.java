package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import com.team2_portfoliomanagement.demo.repository.CashAccountRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.swing.text.html.Option;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

@SpringBootTest
public class CashAccountServiceTest {

    @Autowired
    private CashAccountService cashAccountService;

    @MockBean
    private CashAccountRepository cashAccountRepository;

    @Test
    public void cashAccountServiceFindAllTest(){
        List<CashAccount> allCashAccounts = new ArrayList<CashAccount>();
        CashAccount cashAccount = new CashAccount();


        cashAccount.setId(123);
        allCashAccounts.add(cashAccount);

        when(cashAccountRepository.findAll()).thenReturn(allCashAccounts);

        assertEquals(1, cashAccountService.findAll().size());
    }

    @Test
    public void cashAccountServiceFindByIdTest(){
        HashMap<Integer, Optional<CashAccount>> allCashAccounts = new HashMap<>();
        CashAccount cashAccount = new CashAccount();

        Optional<CashAccount> optionalCashAccount = Optional.of(cashAccount);
        cashAccount.setId(123);
        allCashAccounts.put(123, optionalCashAccount);


        when(cashAccountRepository.findById("123")).thenReturn(allCashAccounts.get(123));

        assertTrue(cashAccountService.findById("123").isPresent());
    }

    @Test
    public void cashAccountServiceSaveTest(){
        CashAccount cashAccount = new CashAccount();

        cashAccount.setId(123);

        when(cashAccountRepository.save(cashAccount)).thenReturn(cashAccount);

        assertEquals(cashAccount.getId(), cashAccountService.save(cashAccount).getId());
    }

    @Test
    public void  cashAccountServiceDeleteTest(){
        HashMap<Integer, CashAccount> allCashAccounts = new HashMap<>();
        CashAccount cashAccount = new CashAccount();

        cashAccount.setId(123);
        allCashAccounts.put(123, cashAccount);

        doNothing().when(cashAccountRepository).deleteById(isA(String.class));
        allCashAccounts.remove(123);

        cashAccountService.delete("123");
        assertEquals(0, allCashAccounts.size());
    }

    @Test
    public void cashAccountServiceUpdateBalanceTest(){
        List<CashAccount> allCashAccounts = new ArrayList<CashAccount>();
        CashAccount cashAccount = new CashAccount();


        cashAccount.setId(123);
        cashAccount.setBalance(1000);
        allCashAccounts.add(cashAccount);

        when(cashAccountRepository.findAll()).thenReturn(new ArrayList<CashAccount>());
        cashAccountService.updateBalance(200);
        assertEquals(1000, cashAccount.getBalance());

        when(cashAccountRepository.findAll()).thenReturn(allCashAccounts);
        cashAccountService.updateBalance(200);
        assertEquals(1200, cashAccount.getBalance());
    }

}
