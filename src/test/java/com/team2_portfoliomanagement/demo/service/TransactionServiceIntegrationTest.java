package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.Transaction;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
public class TransactionServiceIntegrationTest {

    @Autowired
    private TransactionService transactionService;

    @Test
    public void testTransactionServiceFindAll() {
        List<Transaction> allTransactions = transactionService.findAll();

        assertEquals(0, allTransactions.size()); // edit this when there are MongoDB Documents stored
        //System.out.println("AAPL"+ allTransactions.get(0));
    }

}
