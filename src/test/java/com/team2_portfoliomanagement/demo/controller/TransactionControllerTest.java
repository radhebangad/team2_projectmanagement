package com.team2_portfoliomanagement.demo.controller;

import com.team2_portfoliomanagement.demo.model.Transaction;
import com.team2_portfoliomanagement.demo.service.TransactionService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

@SpringBootTest
public class TransactionControllerTest {
    @Autowired
    private TransactionController transactionController;

    @MockBean
    private TransactionService transactionService;

    @Test
    public void transactionServiceFindAllTest(){
        List<Transaction> allTransactions = new ArrayList<Transaction>();
        Transaction testTransaction = new Transaction();

        testTransaction.setId("123");
        allTransactions.add(testTransaction);

        when(transactionService.findAll()).thenReturn(allTransactions);

        assertEquals(1, transactionController.findAll().size());
    }

    @Test
    public void transactionServiceFindByIdTest(){
        HashMap<String, Optional<Transaction>> allTransactions = new HashMap<>();
        Transaction testTransaction = new Transaction();

        Optional<Transaction> optionalTransaction = Optional.of(testTransaction);
        testTransaction.setId("123");
        allTransactions.put("123", optionalTransaction);

        when(transactionService.findById("123")).thenReturn(allTransactions.get("123"));
        assertEquals(HttpStatus.OK, transactionController.findById("123").getStatusCode());


        when(transactionService.findById("123")).thenThrow(NoSuchElementException.class);
        assertEquals(HttpStatus.NOT_FOUND, transactionController.findById("123").getStatusCode());
    }

    @Test
    public void transactionControllerSaveTest(){
        Transaction transaction = new Transaction();

        transaction.setId("123");

        when(transactionService.save(transaction)).thenReturn(transaction);

        assertEquals(transaction.getId(), transactionController.save(transaction).getId());
    }

    @Test
    public void transactionControllerDeleteTest(){
        Transaction transaction = new Transaction();

        transaction.setId("123");

        when(transactionService.findById("123")).thenReturn(Optional.empty());
        assertEquals(HttpStatus.NOT_FOUND, transactionController.delete("123").getStatusCode());

        when(transactionService.findById("123")).thenReturn(Optional.of(transaction));
        assertEquals(HttpStatus.NO_CONTENT, transactionController.delete("123").getStatusCode());
    }

}
