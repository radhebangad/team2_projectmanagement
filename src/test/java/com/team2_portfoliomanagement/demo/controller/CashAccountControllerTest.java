package com.team2_portfoliomanagement.demo.controller;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import com.team2_portfoliomanagement.demo.repository.CashAccountRepository;
import com.team2_portfoliomanagement.demo.service.CashAccountService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;


import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.isA;
import static org.mockito.Mockito.*;

@SpringBootTest
public class CashAccountControllerTest {
    @Autowired
    private CashAccountController cashAccountController;

    @MockBean
    private CashAccountService cashAccountService;

    @MockBean
    private CashAccountRepository cashAccountRepository;

    @Test
    public void cashAccountServiceFindAllTest(){
        List<CashAccount> allCashAccounts = new ArrayList<CashAccount>();
        CashAccount cashAccount = new CashAccount();


        cashAccount.setId(123);
        allCashAccounts.add(cashAccount);

        when(cashAccountService.findAll()).thenReturn(allCashAccounts);

        assertEquals(1, cashAccountController.findAll().size());
    }

    @Test
    public void cashAccountServiceFindByIdTest(){
        HashMap<Integer, Optional<CashAccount>> allCashAccounts = new HashMap<>();
        CashAccount cashAccount = new CashAccount();

        Optional<CashAccount> optionalCashAccount = Optional.of(cashAccount);
        cashAccount.setId(123);
        allCashAccounts.put(123, optionalCashAccount);


        when(cashAccountService.findById("123")).thenReturn(allCashAccounts.get(123));
        assertEquals(HttpStatus.OK, cashAccountController.findById("123").getStatusCode());


        when(cashAccountService.findById("123")).thenThrow(NoSuchElementException.class);
        assertEquals(HttpStatus.NOT_FOUND, cashAccountController.findById("123").getStatusCode());
    }

    @Test
    public void cashAccountControllerSaveTest(){
        CashAccount cashAccount = new CashAccount();

        cashAccount.setId(123);

        when(cashAccountService.save(cashAccount)).thenReturn(cashAccount);

        assertEquals(cashAccount.getId(), cashAccountController.save(cashAccount).getId());
    }

    @Test
    public void cashAccountControllerDeleteTest(){
        CashAccount cashAccount = new CashAccount();

        cashAccount.setId(123);

        when(cashAccountService.findById("123")).thenReturn(Optional.empty());
        assertEquals(HttpStatus.NOT_FOUND, cashAccountController.delete("123").getStatusCode());

        when(cashAccountService.findById("123")).thenReturn(Optional.of(cashAccount));
        assertEquals(HttpStatus.NO_CONTENT, cashAccountController.delete("123").getStatusCode());
    }

    @Test
    public void cashAccountControllerUpdateBalances(){
        doNothing().when(cashAccountService).updateBalance(100);
        cashAccountController.updateBalance(100);
        verify(cashAccountService).updateBalance(100);
    }

}
