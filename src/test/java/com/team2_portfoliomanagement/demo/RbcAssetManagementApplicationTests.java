package com.team2_portfoliomanagement.demo;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@Disabled
@SpringBootTest
class RbcAssetManagementApplicationTests {


    @Test
    void contextLoads() {
        RbcAssetManagementApplication.main(new String[] {});
    }

}
