package com.team2_portfoliomanagement.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RbcAssetManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(RbcAssetManagementApplication.class, args);
    }

}
