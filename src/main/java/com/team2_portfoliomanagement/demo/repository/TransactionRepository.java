package com.team2_portfoliomanagement.demo.repository;

import com.team2_portfoliomanagement.demo.model.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Component;

@Component
public interface TransactionRepository extends MongoRepository<Transaction, String> {
}
