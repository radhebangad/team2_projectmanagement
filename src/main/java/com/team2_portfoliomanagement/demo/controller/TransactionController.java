package com.team2_portfoliomanagement.demo.controller;


import com.team2_portfoliomanagement.demo.model.Transaction;
import com.team2_portfoliomanagement.demo.service.TransactionServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;
@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/stock")

public class TransactionController {

    @Autowired
    private TransactionServiceInterface stockService;

    @GetMapping
    public List<Transaction> findAll() { return stockService.findAll(); }

    @GetMapping("{id}")
    public ResponseEntity<Transaction> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<Transaction>(stockService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public Transaction save(@RequestBody Transaction transaction) {
        return stockService.save(transaction);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(stockService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        stockService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }
}
