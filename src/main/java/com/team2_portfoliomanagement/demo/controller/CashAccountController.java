package com.team2_portfoliomanagement.demo.controller;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import com.team2_portfoliomanagement.demo.service.CashAccountServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.NoSuchElementException;

@CrossOrigin("*")
@RestController
@RequestMapping("/api/v1/cashaccounts")
public class CashAccountController {

    @Autowired
    CashAccountServiceInterface cashAccountService;

    @GetMapping
    public List<CashAccount> findAll() {
        return cashAccountService.findAll();
    }

    @GetMapping("{id}")
    public ResponseEntity<CashAccount> findById(@PathVariable String id) {
        try {
            return new ResponseEntity<CashAccount>(cashAccountService.findById(id).get(), HttpStatus.OK);
        } catch (NoSuchElementException ex) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }
    }

    @PostMapping
    public CashAccount save(@RequestBody CashAccount cashAccount) {
        return cashAccountService.save(cashAccount);
    }

    @DeleteMapping("{id}")
    public ResponseEntity delete(@PathVariable String id) {
        if(cashAccountService.findById(id).isEmpty()) {
            return new ResponseEntity(HttpStatus.NOT_FOUND);
        }

        cashAccountService.delete(id);

        return new ResponseEntity(HttpStatus.NO_CONTENT);
    }

    @PostMapping("{amount}")
    public void updateBalance(@PathVariable int amount){
        cashAccountService.updateBalance(amount);
    }

}
