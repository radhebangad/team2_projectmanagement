package com.team2_portfoliomanagement.demo.model;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
@Getter
@Setter

public class Transaction {

    @Id
    private String id;
    private String ticker;
    private double orderPrice;
    private int orderSize;
    private String orderType;
}
