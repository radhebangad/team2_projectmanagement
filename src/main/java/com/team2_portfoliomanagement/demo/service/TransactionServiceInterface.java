package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.Transaction;

import java.util.List;
import java.util.Optional;

public interface TransactionServiceInterface {
    List<Transaction> findAll();

    Transaction save(Transaction transaction);

    Optional<Transaction> findById(String id);
    void delete(String id);
}
