package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;


public interface CashAccountServiceInterface {
    Optional<CashAccount> findById(String id);
    List<CashAccount> findAll();
    void delete(String id);
    void updateBalance(int amount);
    CashAccount save(CashAccount cashAccount);
}
