package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.Transaction;
import com.team2_portfoliomanagement.demo.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class TransactionService implements TransactionServiceInterface {

    @Autowired
    private TransactionRepository transactionRepository;

    public List<Transaction> findAll() {
        return transactionRepository.findAll();
    }

    public Optional<Transaction> findById(String id) {
        return transactionRepository.findById(id);
    }

    public Transaction save(Transaction transaction) {

        return transactionRepository.save(transaction);
    }

    public void delete(String id) {
        transactionRepository.deleteById(id);
    }
}
