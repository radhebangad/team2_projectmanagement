package com.team2_portfoliomanagement.demo.service;

import com.team2_portfoliomanagement.demo.model.CashAccount;
import com.team2_portfoliomanagement.demo.repository.CashAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.swing.text.html.Option;
import java.util.List;
import java.util.Optional;

@Service
public class CashAccountService implements CashAccountServiceInterface{

    @Autowired
    private CashAccountRepository cashAccountRepository;

    @Override
    public List<CashAccount> findAll(){
        return cashAccountRepository.findAll();
    }

    @Override
    public Optional<CashAccount> findById(String id){
        return cashAccountRepository.findById(id);
    }

    @Override
    public void delete(String id){
        cashAccountRepository.deleteById(id);
    }

    @Override
    public CashAccount save(CashAccount cashAccount){
        return cashAccountRepository.save(cashAccount);
    }

    @Override
    public void updateBalance(int amount){
        List<CashAccount> cashAccount = cashAccountRepository.findAll();
        if (!cashAccount.isEmpty()){
            cashAccount.get(0).setBalance(cashAccount.get(0).getBalance()+amount);
            cashAccountRepository.save(cashAccount.get(0));
        }
    }

}
